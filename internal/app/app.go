package app

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"gitlab.com/blankspace9/biocad-parser/internal/config"
	"gitlab.com/blankspace9/biocad-parser/internal/delivery/rest"
	"gitlab.com/blankspace9/biocad-parser/internal/repository"
	"gitlab.com/blankspace9/biocad-parser/internal/service"
	"gitlab.com/blankspace9/biocad-parser/pkg/httpserver"
	"gitlab.com/blankspace9/biocad-parser/pkg/postgres"
)

func Run(folder, filename string) {
	cfg, err := config.NewConfig(folder, filename)
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	SetLogger(cfg.Log.Level)

	// Init postgres connection
	log.Info("Init postgres db...")
	pg, err := postgres.NewPostgresConnection(postgres.PostgresConnectionInfo{
		Host:         cfg.DB.Host,
		Port:         cfg.DB.Port,
		Username:     cfg.DB.Username,
		DBName:       cfg.DB.DB,
		SSLMode:      cfg.DB.SSLMode,
		Password:     cfg.DB.Password,
		MaxOpenConns: cfg.DB.MaxOpenConns,
	})
	if err != nil {
		log.Fatalf("Run app - Init postgres db: %s", err)
	}
	defer pg.Close()

	// Init repositories
	log.Info("Initializing repositories...")
	repositories := repository.NewRepositories(pg)

	// Init dependencies
	log.Info("Initializing dependencies...")
	deps := service.ServiceDependencies{
		Repos:               repositories,
		Dir:                 cfg.DirManager.Dir,
		DirCheckInterval:    cfg.DirManager.Interval,
		NumParserThreads:    cfg.Parser.NumThreads,
		NumFields:           cfg.Parser.NumFields,
		OutputDir:           cfg.Generator.OutputDir,
		Format:              cfg.Generator.Format,
		NumGeneratorThreads: cfg.Generator.NumThreads,
		ParseQueue:          make(chan string, cfg.Queues.ParseQueueBuffer),
		GenerateQueue:       make(chan string, cfg.Queues.GenerateQueueBuffer),
	}

	// Init services
	log.Info("Initializing services...")
	services := service.NewServices(deps)

	// Start DirManager
	log.Info("Starting DirManager...")
	go services.DirManager.Start()
	defer services.DirManager.Stop()

	// Start Parser
	log.Info("Starting Parser...")
	go services.Parser.Start()
	defer services.Parser.Stop()

	// Start Generator
	log.Info("Starting Generator")
	go services.Generator.Start()
	defer services.Generator.Stop()

	// init handler
	log.Info("Initializing handler...")
	handler := rest.NewHandler(*services)

	// init & run server
	log.Info("Starting http server...")
	log.Debugf("Server port: %s", cfg.Server.Port)
	// TODO: handler
	httpServer := httpserver.NewServer(handler.InitRouter(), httpserver.Port(cfg.Server.Port))

	log.Info("Server started")

	// graceful shutdown
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-exit:
		log.Info("Run app - signal: " + s.String())
	case err = <-httpServer.Notify():
		log.Errorf("Run app - httpServer.Notify: %s", err)
	}

	log.Info("Shutting down...")
	err = httpServer.Shutdown()
	if err != nil {
		log.Errorf("Run app - httpServer.Shutdown: %s", err)
	}

	log.Info("Server gracefully stopped")
}
