package rest

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/blankspace9/biocad-parser/internal/service"
)

type Handler struct {
	messageService service.Messager
}

func NewHandler(services service.Services) *Handler {
	return &Handler{
		messageService: services.Messager,
	}
}

func (h *Handler) InitRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/api/messages", h.getMessages).Methods(http.MethodGet)

	return r
}
