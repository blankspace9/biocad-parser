package rest

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
)

func (h *Handler) getMessages(w http.ResponseWriter, r *http.Request) {
	guidString := r.URL.Query().Get("guid")
	guid, err := uuid.Parse(guidString)
	if err != nil {
		http.Error(w, "Failed to get query parametr guid", http.StatusBadRequest)
		log.Error("History - getReport - Failed to get query parametr:", err)
		return
	}
	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil {
		http.Error(w, "Failed to get query parametr page", http.StatusBadRequest)
		log.Error("History - getReport - Failed to get query parametr:", err)
		return
	}
	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		http.Error(w, "Failed to get query parametr limit", http.StatusBadRequest)
		log.Error("History - getReport - Failed to get query parametr:", err)
		return
	}

	messages, err := h.messageService.GetPaginateData(r.Context(), guid, page, limit)
	if err != nil {
		http.Error(w, "Failed to get messages by guid", http.StatusInternalServerError)
		log.Error("History - getReport - Failed to get messages:", err)
		return
	}

	messageMap := make(map[string]domain.Message)
	for i, v := range messages {
		key := fmt.Sprintf("%d", (page-1)*limit+1+i)
		messageMap[key] = v
	}

	response, err := json.Marshal(messageMap)
	if err != nil {
		http.Error(w, "Failed to encode response JSON", http.StatusInternalServerError)
		log.Error("History - getReport - Failed to get messages:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}
