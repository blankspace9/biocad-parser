package config

import (
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/spf13/viper"
)

type (
	Config struct {
		DB Postgres

		Server struct {
			Port string `mapstructure:"port"`
		} `mapstructure:"server"`

		Log struct {
			Level string `mapstructure:"level"`
		} `mapstructure:"log"`

		DirManager struct {
			Dir      string        `mapstructure:"dir"`
			Interval time.Duration `mapstructure:"interval"`
		} `mapstructure:"dirmanager"`

		Queues struct {
			ParseQueueBuffer    int `mapstructure:"parseQueueBuffer"`
			GenerateQueueBuffer int `mapstructure:"generateQueueBuffer"`
		} `mapstructure:"queues"`

		Parser struct {
			NumThreads int `mapstructure:"threads"`
			NumFields  int `mapstructure:"fields"`
		} `mapstructure:"parser"`

		Generator struct {
			OutputDir  string `mapstructure:"outputdir"`
			Format     string `mapstructure:"format"`
			NumThreads int    `mapstructure:"threads"`
		} `mapstructure:"generator"`
	}

	Postgres struct {
		Host         string
		Port         int
		Username     string
		DB           string
		SSLMode      string
		Password     string
		MaxOpenConns int
	}
)

func NewConfig(folder, filename string) (*Config, error) {
	cfg := new(Config)

	if os.Getenv("DOCKER_ENV") != "true" {
		err := godotenv.Load(".env")
		if err != nil {
			return nil, err
		}
	}

	viper.AddConfigPath(folder)
	viper.SetConfigName(filename)

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	if err := viper.Unmarshal(cfg); err != nil {
		return nil, err
	}

	if err := envconfig.Process("postgres", &cfg.DB); err != nil {
		return nil, err
	}

	return cfg, nil
}
