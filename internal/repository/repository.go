package repository

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
	"gitlab.com/blankspace9/biocad-parser/internal/repository/postgres"
)

type Message interface {
	Create(ctx context.Context, unit domain.Message) (int64, error)
	GetByGuid(ctx context.Context, guid uuid.UUID) ([]domain.Message, error)
	GetPaginateByGuid(ctx context.Context, guid uuid.UUID, page, limit int) ([]domain.Message, error)
}

type File interface {
	Create(ctx context.Context, file domain.File) (int64, error)
	GetByFilename(ctx context.Context, filename string) (*domain.File, error)
}

type Error interface {
	Create(ctx context.Context, err domain.Error) (int64, error)
}

type Repositories struct {
	Message
	File
	Error
}

func NewRepositories(db *sql.DB) *Repositories {
	return &Repositories{
		Message: postgres.NewMessageRepo(db),
		File:    postgres.NewFileRepo(db),
		Error:   postgres.NewErrorRepo(db),
	}
}
