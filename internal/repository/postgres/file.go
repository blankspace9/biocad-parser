package postgres

import (
	"context"
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
	"gitlab.com/blankspace9/biocad-parser/internal/repository/repoerrors"
)

type FileRepo struct {
	db *sql.DB
}

func NewFileRepo(db *sql.DB) *FileRepo {
	return &FileRepo{db}
}

func (fr *FileRepo) Create(ctx context.Context, file domain.File) (int64, error) {
	var id int64
	err := fr.db.QueryRow("INSERT INTO files (filename, process_date) values ($1, $2) RETURNING id", file.Filename, pq.FormatTimestamp(file.ProcessDate)).Scan(&id)
	if err != nil {
		return -1, err
	}

	return id, nil
}

func (fr *FileRepo) GetByFilename(ctx context.Context, filename string) (*domain.File, error) {
	var f domain.File
	err := fr.db.QueryRow("SELECT id, filename, process_date FROM files WHERE filename=$1", filename).Scan(&f.ID, &f.Filename, &f.ProcessDate)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, repoerrors.ErrNotFound
		}
		return nil, err
	}

	return &f, nil
}
