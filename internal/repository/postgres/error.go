package postgres

import (
	"context"
	"database/sql"

	"gitlab.com/blankspace9/biocad-parser/internal/domain"
)

type ErrorRepo struct {
	db *sql.DB
}

func NewErrorRepo(db *sql.DB) *ErrorRepo {
	return &ErrorRepo{db}
}

func (er *ErrorRepo) Create(ctx context.Context, errInfo domain.Error) (int64, error) {
	var id int64
	err := er.db.QueryRow("INSERT INTO errors (filename, text) values ($1, $2) RETURNING id", errInfo.Filename, errInfo.Text).Scan(&id)
	if err != nil {
		return -1, err
	}

	return id, nil
}
