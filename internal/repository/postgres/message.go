package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
)

type MessageRepo struct {
	db *sql.DB
}

func NewMessageRepo(db *sql.DB) *MessageRepo {
	return &MessageRepo{db}
}

func (ur *MessageRepo) Create(ctx context.Context, unit domain.Message) (int64, error) {
	var id int64
	err := ur.db.QueryRow("INSERT INTO messages (n, mqtt, invid, unit_guid, msg_id, text, context, class, level, addr, block, type, bit, invert_bit, created_at) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING id", unit.N, unit.Mqtt, unit.Invid, unit.UnitGuid, unit.MsgID, unit.Text, unit.Context, unit.Class, unit.Level, unit.Addr, unit.Block, unit.Type, unit.Bit, unit.InvertBit, pq.FormatTimestamp(unit.CreatedAt)).Scan(&id)
	if err != nil {
		return -1, err
	}

	return id, nil
}

func (ur *MessageRepo) GetByGuid(ctx context.Context, guid uuid.UUID) ([]domain.Message, error) {
	var messages []domain.Message
	rows, err := ur.db.Query("SELECT id, n, mqtt, invid, msg_id, text, context, class, level, addr, block, type, bit, invert_bit, created_at FROM messages WHERE unit_guid=$1 ORDER BY created_at DESC", guid)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var message domain.Message
		err := rows.Scan(&message.ID, &message.N, &message.Mqtt, &message.Invid, &message.MsgID, &message.Text, &message.Context, &message.Class, &message.Level, &message.Addr, &message.Block, &message.Type, &message.Bit, &message.InvertBit, &message.CreatedAt)
		if err != nil {
			return nil, err
		}
		messages = append(messages, message)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return messages, nil
}

func (ur *MessageRepo) GetPaginateByGuid(ctx context.Context, guid uuid.UUID, page, limit int) ([]domain.Message, error) {
	var messages []domain.Message
	offset := (page - 1) * limit
	rows, err := ur.db.Query("SELECT id, n, mqtt, invid, unit_guid, msg_id, text, context, class, level, addr, block, type, bit, invert_bit, created_at FROM messages WHERE unit_guid=$1 ORDER BY created_at DESC LIMIT $2 OFFSET $3", guid, limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var message domain.Message
		err := rows.Scan(&message.ID, &message.N, &message.Mqtt, &message.Invid, &message.UnitGuid, &message.MsgID, &message.Text, &message.Context, &message.Class, &message.Level, &message.Addr, &message.Block, &message.Type, &message.Bit, &message.InvertBit, &message.CreatedAt)
		if err != nil {
			return nil, err
		}
		messages = append(messages, message)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return messages, nil
}
