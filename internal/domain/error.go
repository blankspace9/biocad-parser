package domain

type Error struct {
	ID       int64
	Filename string
	Text     string
}
