package domain

import "time"

type File struct {
	ID          int64
	Filename    string
	ProcessDate time.Time
}
