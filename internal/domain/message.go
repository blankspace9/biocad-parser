package domain

import (
	"time"

	"github.com/google/uuid"
)

type Message struct {
	ID        int64     `json:"id"`
	N         int       `json:"n"`
	Mqtt      string    `json:"mqtt"`
	Invid     string    `json:"invid"`
	UnitGuid  uuid.UUID `json:"unit_guid"`
	MsgID     string    `json:"msg_id"`
	Text      string    `json:"text"`
	Context   string    `json:"context"`
	Class     string    `json:"class"`
	Level     int       `json:"level"`
	Area      string    `json:"area"`
	Addr      string    `json:"addr"`
	Block     string    `json:"block"`
	Type      string    `json:"type"`
	Bit       string    `json:"bit"`
	InvertBit string    `json:"invert_bit"`
	CreatedAt time.Time `json:"createdAt"`
}
