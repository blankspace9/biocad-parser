package service

import (
	"context"
	"fmt"
	"os"
	"sync"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
	"gitlab.com/blankspace9/biocad-parser/internal/repository"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

const template = "Created at: %s\nId: %d\nN: %d\nMQTT: %s\nInvId: %s\nMsgId: %s\nText: %s\nContext: %s\nClass: %s\nLevel: %d\nAddr: %s\nBlock: %s\nType: %s\nBit: %s\nInvertBit: %s"

type GeneratorService struct {
	messageRepo   repository.Message
	outputDir     string
	format        string
	numThreads    int
	generateQueue chan string
	wg            sync.WaitGroup
	stopChan      chan struct{}
}

func NewGeneratorService(messageRepo repository.Message, outputDir, format string, numThreads int, generateQueue chan string) *GeneratorService {
	return &GeneratorService{
		messageRepo:   messageRepo,
		outputDir:     outputDir,
		format:        format,
		numThreads:    numThreads,
		generateQueue: generateQueue,
		stopChan:      make(chan struct{}),
	}
}

func (g *GeneratorService) Start() {
	for i := 0; i < g.numThreads; i++ {
		g.wg.Add(1)
		go g.worker()
	}
	for {
		select {
		case guid := <-g.generateQueue:
			go func(newGuid string) {
				g.generateQueue <- newGuid
			}(guid)
		case <-g.stopChan:
			g.wg.Wait()
			return
		}
	}
}

func (g *GeneratorService) Stop() {
	log.Infof("Stop Generator Service")
	close(g.stopChan)
}

func (g *GeneratorService) worker() {
	defer g.wg.Done()
	for guidString := range g.generateQueue {
		guid, err := uuid.Parse(guidString)
		if err != nil {
			log.Error("Generator - worker - " + err.Error())
			continue
		}
		messages, err := g.messageRepo.GetByGuid(context.TODO(), guid)
		if err != nil {
			log.Error("Generator - worker - " + err.Error())
			continue
		}

		err = g.generateTextFile(guidString, messages)
		if err != nil {
			log.Error("Generator - worker - " + err.Error())
			continue
		}

		log.Info("Output file for unit " + guidString + " was created")
	}
}

func (g *GeneratorService) generateTextFile(filename string, messages []domain.Message) error {
	file, err := os.Create(fmt.Sprintf("%s/%s.%s", g.outputDir, filename, g.format))
	if err != nil {
		return err
	}
	defer file.Close()

	p := message.NewPrinter(language.Und)

	p.Fprintf(file, "Total: %d messages\n\n", len(messages))

	for index, msg := range messages {
		p.Fprintf(file, "----------------------------------------------------------------------\n", len(messages))
		p.Fprintf(file, template, msg.CreatedAt.String(), msg.ID, msg.N, msg.Mqtt, msg.Invid, msg.MsgID, msg.Text, msg.Context, msg.Class, msg.Level, msg.Addr, msg.Block, msg.Type, msg.Bit, msg.InvertBit)
		if index != len(messages)-1 {
			p.Fprintf(file, "\n\n")
		}
	}

	return nil
}
