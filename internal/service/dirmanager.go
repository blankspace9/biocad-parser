package service

import (
	"context"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/blankspace9/biocad-parser/internal/repository"
	"gitlab.com/blankspace9/biocad-parser/internal/repository/repoerrors"
)

type DirManagerService struct {
	fileRepo    repository.File
	dir         string
	interval    time.Duration
	queue       chan string
	queueBuffer int
	stopChan    chan struct{}
}

func NewDirManagerService(fileRepo repository.File, dir string, interval time.Duration, queue chan string) *DirManagerService {
	return &DirManagerService{
		fileRepo: fileRepo,
		dir:      dir,
		interval: interval,
		queue:    queue,
		stopChan: make(chan struct{}),
	}
}

func (dm *DirManagerService) Start() {
	ticker := time.NewTicker(dm.interval)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			files, err := os.ReadDir(dm.dir)
			if err != nil {
				log.Errorf("Directory Manager error: %s", err)
				continue
			}

			for _, file := range files {
				if filepath.Ext(file.Name()) == ".tsv" {
					_, err := dm.fileRepo.GetByFilename(context.TODO(), file.Name())
					if err != nil {
						if err == repoerrors.ErrNotFound {
							dm.queue <- file.Name()
						} else {
							log.Errorf("Directory Manager - getting file from DB error: %s", err)
						}
					}
				}
			}
			log.Info("Unproccessed files added to queue")
		case <-dm.stopChan:
			return
		}
	}
}

func (dm *DirManagerService) Stop() {
	log.Infof("Stop Directory Manager")
	close(dm.stopChan)
}
