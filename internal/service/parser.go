package service

import (
	"bufio"
	"context"
	"errors"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
	"gitlab.com/blankspace9/biocad-parser/internal/repository"
)

type ParserService struct {
	messageRepo   repository.Message
	fileRepo      repository.File
	errorRepo     repository.Error
	dir           string
	numThreads    int
	numFields     int
	parseQueue    chan string
	generateQueue chan string
	wg            sync.WaitGroup
	stopChan      chan struct{}
}

func NewParserService(messageRepo repository.Message, fileRepo repository.File, errorRepo repository.Error, dir string, numThreads, numFields int, parseQueue, generateQueue chan string) *ParserService {
	return &ParserService{
		messageRepo:   messageRepo,
		fileRepo:      fileRepo,
		errorRepo:     errorRepo,
		dir:           dir,
		numThreads:    numThreads,
		numFields:     numFields,
		parseQueue:    parseQueue,
		generateQueue: generateQueue,
		stopChan:      make(chan struct{}),
	}
}

func (p *ParserService) Start() {
	for i := 0; i < p.numThreads; i++ {
		p.wg.Add(1)
		go p.worker()
	}
	for {
		select {
		case filename := <-p.parseQueue:
			go func(fname string) {
				p.parseQueue <- fname
			}(filename)
		case <-p.stopChan:
			p.wg.Wait()
			return
		}
	}
}

func (p *ParserService) Stop() {
	log.Infof("Stop Parser Service")
	close(p.stopChan)
}

func (p *ParserService) worker() {
	defer p.wg.Done()
	guids := make(map[string]struct{})
	for filename := range p.parseQueue {
		file, err := os.Open(p.dir + "/" + filename)
		if err != nil {
			p.addFile(filename)
			p.addError(filename, err)
			log.Error(`Parser - worker - File "` + filename + `" cannot be parsed. ` + err.Error())
			continue
		}

		scanner := bufio.NewScanner(file)

		scanner.Scan()
		if len(strings.Split(scanner.Text(), "\t")) == p.numFields {
			p.addFile(filename)
			for scanner.Scan() {
				var rowErrs []error
				var message domain.Message
				message, rowErrs = p.parseRow(scanner.Text())
				if len(rowErrs) == 0 {
					p.addMessage(message)
					guids[message.UnitGuid.String()] = struct{}{}
				}
				for _, rowErr := range rowErrs {
					p.addError(filename, rowErr)
				}
			}
			if err := scanner.Err(); err != nil {
				p.addError(filename, err)
			}
			log.Info(`File "` + filename + `" was parsed`)
			for guid := range guids {
				p.generateQueue <- guid
			}
		} else {
			p.addFile(filename)
			p.addError(filename, errors.New("Invalid file structure"))
			log.Error(`Parser - worker - File "` + filename + `" cannot be parsed. Invalid structure`)
		}

		// TODO: link with file generator (observer or event bus) with guids map
		file.Close()
	}
}

func (p *ParserService) addError(filename string, err error) {
	p.errorRepo.Create(context.TODO(), domain.Error{
		Filename: filename,
		Text:     err.Error(),
	})
}

func (p *ParserService) addFile(filename string) error {
	_, err := p.fileRepo.Create(context.TODO(), domain.File{
		Filename:    filename,
		ProcessDate: time.Now(),
	})

	return err
}

func (p *ParserService) addMessage(message domain.Message) error {
	_, err := p.messageRepo.Create(context.TODO(), message)

	return err
}

func (p *ParserService) parseRow(row string) (domain.Message, []error) {
	var errs []error
	var message domain.Message
	fields := strings.Split(row, "\t")

	n, err := strconv.Atoi(fields[0])
	if err != nil {
		errs = append(errs, errors.New("Parsing row with number "+fields[0]+" error. Invalid number"))
	}

	// guid, err := guid.ParseString(fields[3])
	uuid, err := uuid.Parse(fields[3])
	if err != nil {
		errs = append(errs, errors.New("Parsing row with number "+fields[0]+" error. Invalid unit-guid"))
	}

	level, err := strconv.Atoi(fields[8])
	if err != nil {
		errs = append(errs, errors.New("Parsing row with number "+fields[0]+" error. Invalid level"))
	}

	if err == nil {
		message = domain.Message{
			N:         n,
			Mqtt:      fields[1],
			Invid:     fields[2],
			UnitGuid:  uuid,
			MsgID:     fields[4],
			Text:      fields[5],
			Context:   fields[6],
			Class:     fields[7],
			Level:     level,
			Area:      fields[9],
			Addr:      fields[10],
			Block:     fields[11],
			Type:      fields[12],
			Bit:       fields[13],
			InvertBit: fields[14],
			CreatedAt: time.Now(),
		}
	}

	return message, errs
}
