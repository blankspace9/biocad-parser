package service

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
	"gitlab.com/blankspace9/biocad-parser/internal/repository"
)

type DirManager interface {
	Start()
	Stop()
}

type Parser interface {
	Start()
	Stop()
}

type Generator interface {
	Start()
	Stop()
}

type Messager interface {
	GetPaginateData(ctx context.Context, guid uuid.UUID, page, limit int) ([]domain.Message, error)
}

type ServiceDependencies struct {
	Repos               *repository.Repositories
	Dir                 string
	DirCheckInterval    time.Duration
	NumParserThreads    int
	NumFields           int
	OutputDir           string
	Format              string
	NumGeneratorThreads int
	ParseQueue          chan string
	GenerateQueue       chan string
}

type Services struct {
	DirManager
	Parser
	Generator
	Messager
}

func NewServices(deps ServiceDependencies) *Services {
	return &Services{
		DirManager: NewDirManagerService(deps.Repos.File, deps.Dir, deps.DirCheckInterval, deps.ParseQueue),
		Parser:     NewParserService(deps.Repos.Message, deps.Repos.File, deps.Repos.Error, deps.Dir, deps.NumParserThreads, deps.NumFields, deps.ParseQueue, deps.GenerateQueue),
		Generator:  NewGeneratorService(deps.Repos.Message, deps.OutputDir, deps.Format, deps.NumGeneratorThreads, deps.GenerateQueue),
		Messager:   NewMessageService(deps.Repos.Message),
	}
}
