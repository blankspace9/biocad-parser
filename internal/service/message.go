package service

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/blankspace9/biocad-parser/internal/domain"
	"gitlab.com/blankspace9/biocad-parser/internal/repository"
)

type MessageService struct {
	messageRepo repository.Message
}

func NewMessageService(messageRepo repository.Message) *MessageService {
	return &MessageService{
		messageRepo: messageRepo,
	}
}

func (m *MessageService) GetPaginateData(ctx context.Context, guid uuid.UUID, page, limit int) ([]domain.Message, error) {
	messages, err := m.messageRepo.GetPaginateByGuid(ctx, guid, page, limit)
	if err != nil {
		return nil, err
	}

	return messages, nil
}
