package main

import "gitlab.com/blankspace9/biocad-parser/internal/app"

const (
	CONGIG_DIR  = "configs"
	CONFIG_FILE = "config"
)

func main() {
	app.Run(CONGIG_DIR, CONFIG_FILE)
}
