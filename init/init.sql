DROP TABLE IF EXISTS messages, files, errors;

CREATE TABLE messages (
    id SERIAL PRIMARY KEY,
    n INTEGER,
    mqtt VARCHAR(255),
    invid VARCHAR(8) CHECK(invid ~ '^G-\d{6}$'),
    unit_guid UUID NOT NULL,
    msg_id VARCHAR(64) CHECK(msg_id!=''),
    text TEXT CHECK(text!=''),
    context VARCHAR(255),
    class VARCHAR(16) CHECK(class IN ('alarm', 'working', 'warning', 'waiting', 'info', 'event', 'comand')),
    level INTEGER,
    addr VARCHAR(64) CHECK(addr!=''),
    block VARCHAR(64),
    type VARCHAR(64),
    bit VARCHAR(8),
    invert_bit VARCHAR(8),
    created_at TIMESTAMP
);

CREATE TABLE files (
    id SERIAL PRIMARY KEY,
    filename VARCHAR(255) UNIQUE CHECK(filename!=''),
    process_date TIMESTAMP
);

CREATE TABLE errors (
    id SERIAL PRIMARY KEY,
    filename VARCHAR(255) REFERENCES files(filename) ON DELETE CASCADE,
    text TEXT
);