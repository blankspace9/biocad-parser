# biocad-parser

Сервис для парсинга статичных .tsv файлов с помещением в базу данных.
  
Используемые технологии:  
- PostgreSQL
- gorilla/mux
- pq
- logrus
- viper
  
## Description  
Сервис был написан с использованием чистой архитектуры. Были реализованы Graceful Shutdown для корректного завершения работы, worker pool для многопоточного парсинга и генерации файлов.  
  
Данный сервис на старте запускает три горутины, каждая для определенного "сервиса":  
* **DirManager** - просматривает директорию и добавляет необработанные файлы (имена) в канал (очередь) парсинга.  
* **Parser** - ждет пока в очереди не появятся новые файлы. При появлении новых файлов распределяет "воркерам" задачи. Каждый "воркер" парсит определенный файл. После успешного парсинга отправляет guid в канал генерации.  
* **Generator** - ждет пока в очереди не появятся guid, для которых нужно сгенерировать файл. При появлении новых guid распределяет "воркерам" задачи. Каждый "воркер" генерирует файл для определенного guid.  

Помимо этого, в сервисе реализован http-сервер и предоставлен API для получения данных с пагинацией (см. API).

## Getting started

Для локального запуска необходимо создать в postgresql базу данных и запустить скрипт init/init.sql.  
Также необходимо указать конфигурацию проекта.  
Запускается проект выполнением команды 
```bash
go run cmd/app/main.go
```

## Configuration  
### env  
В файле .env задаются конфигурации для базы данных, а также порт http-сервера.
### yaml
* менеджер директории: путь к директории, интервал между каждым просмотром директории.  
* парсер: количество потоков, количество полей во входном файле.  
* генератор файлов: выходная директория, формат выходного файла (только doc), количество потоков.
* очереди: размеры буфферов для каналов, содержащих очерель файлов на парсинг и на генерацию.  

## API

В API реализован один HTTP-метод GET для получения данных с пагинацией.
Пример запроса:  
```bash
curl --location 'localhost:8080/api/messages?guid=01749246-95f6-57db-b7c3-2ae0e8be671f&page=2&limit=2'
```  
Пример ответа:  
```json
{  
	"3": {
        "id": 15,
        "n": 16,
        "mqtt": "",
        "invid": "G-044322",
        "unit_guid": "01749246-95f6-57db-b7c3-2ae0e8be671f",
        "msg_id": "test_alarm",
        "text": "Тест Аларм",
        "context": "",
        "class": "alarm",
        "level": 100,
        "area": "",
        "addr": "TestingForMsg.Alarm",
        "block": "",
        "type": "",
        "bit": "",
        "invert_bit": "",
        "createdAt": "2023-11-26T21:17:08.639843Z"
    },
    "4": {
        "id": 3,
        "n": 4,
        "mqtt": "",
        "invid": "G-044322",
        "unit_guid": "01749246-95f6-57db-b7c3-2ae0e8be671f",
        "msg_id": "cold7_Temp_Al_HH",
        "text": "Высокая температура",
        "context": "",
        "class": "alarm",
        "level": 100,
        "area": "",
        "addr": "cold7_status.Temp_Al_HH",
        "block": "",
        "type": "",
        "bit": "",
        "invert_bit": "",
        "createdAt": "2023-11-26T21:17:08.62609Z"
    }
}
```
